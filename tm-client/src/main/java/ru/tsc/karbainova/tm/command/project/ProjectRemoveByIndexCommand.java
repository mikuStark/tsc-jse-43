package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class ProjectRemoveByIndexCommand extends AbstractCommand {
    @Override
    public String name() {
        return "remove-by-index-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by index";
    }

    @Override
    public void execute() {

        final String index = TerminalUtil.nextLine();
        SessionDTO session = serviceLocator.getSession();
//        serviceLocator.getProjectEndpoint().removeByIndexProject(session, Integer.parseInt(index));
    }

}
