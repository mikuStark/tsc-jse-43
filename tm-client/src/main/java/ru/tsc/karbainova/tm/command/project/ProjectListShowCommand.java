package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.endpoint.SessionDTO;

public class ProjectListShowCommand extends AbstractCommand {
    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        System.out.println("Project list");
        SessionDTO session = serviceLocator.getSession();
        serviceLocator.getProjectEndpoint().findAllProject(session).stream().forEach(o -> System.out.println(o.getName()));
        System.out.println("[OK]");
    }
}
