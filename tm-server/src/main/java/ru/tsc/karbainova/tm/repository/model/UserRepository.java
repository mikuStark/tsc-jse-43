package ru.tsc.karbainova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.karbainova.tm.api.repository.model.IUserRepository;
import ru.tsc.karbainova.tm.model.User;
import ru.tsc.karbainova.tm.repository.dto.AbstractDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDTO e")
                .executeUpdate();
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", User.class).getResultList();
    }

    @Override
    public User findById(@Nullable final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@Nullable final String id) {
        User reference = entityManager.getReference(User.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        entityManager
                .createQuery("DELETE FROM UserDTO e WHERE e.login = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class)
                .getSingleResult()
                .intValue();
    }

}
