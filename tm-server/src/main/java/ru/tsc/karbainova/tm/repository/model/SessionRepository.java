package ru.tsc.karbainova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.karbainova.tm.api.repository.model.ISessionRepository;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.repository.dto.AbstractDTORepository;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO e")
                .executeUpdate();
    }

    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e",
                Session.class).getResultList();
    }

    @Override
    public List<Session> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId",
                        Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Session findById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void removeById(@Nullable final String id) {
        Session reference = entityManager.getReference(Session.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDTO e", Long.class)
                .getSingleResult()
                .intValue();
    }

}
