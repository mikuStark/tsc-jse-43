package ru.tsc.karbainova.tm.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tm_session")
public final class SessionDTO extends AbstractDTOEntity implements Cloneable {

    public SessionDTO() {
    }

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Getter
    @Setter
    @Column
    private Long timestamp;

    @Getter
    @Setter
    @Column(name = "user_id")
    private String userId;

    @Getter
    @Setter
    @Column
    private String signature;


}
