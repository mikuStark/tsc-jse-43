package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.model.IProjectRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.model.IProjectServiceModel;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectServiceModel {

    public ProjectService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<Project> collection) {
        if (collection == null) return;
        for (Project i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public Project add(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    @SneakyThrows
    public void create(@NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (project == null) throw new ProjectNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.removeByIdAndUserId(userId, project.getId());
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Project updateById(@NonNull String userId, @NonNull String id,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable final Project project = findByName(userId, name);
        if (project == null) throw new EntityNotFoundException();
        try {
            project.setName(name);
            project.setDescription(description);
            final IProjectRepository repository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NonNull
    @SneakyThrows
    public Project findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }
}
